<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\testuser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return testuser[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $data = DB::table('testusers')->take(5)->get();
        return new \App\Http\Resources\CustomerCollection($data);
       // return new \App\Http\Resources\CustomerCollection(testuser::latest()->get());
        //return response()->json($customer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
        ]);
        $customer = new testuser();
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->save();
       return new \App\Http\Resources\testuser($customer);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return CustomerResource
     */
    public function show($id)
    {
        return new \App\Http\Resources\testuser(testuser::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);
        $customer = testuser::findOrfail($id);
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->save();
        return new \App\Http\Resources\testuser(testuser::findOrFail($id));
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
